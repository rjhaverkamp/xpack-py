from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError


class EmptyValuesError(Exception):
    pass


class NotCreated(Exception):
    pass


class Elastic:
    def __init__(self, host, username, password, verify_certs=True):
        self.client = Elasticsearch(hosts=host,
                                    http_auth=(username, password), verify_certs=verify_certs)
        self.roles = self._Roles(self)
        self.users = self._Users(self)

    class _Roles:
        def __init__(self, client):
            self.client = client.client

        def get_role(self, role=''):
            if role == '':
                raise EmptyValuesError("empty string in role name")
            try:
                x = self.client.xpack.security.get_role(role)
                return {'exists': True}, x.get(role, '')
            except NotFoundError:
                return {'exists': False}, {}

        def add_role(self, role_name='', indices=[], cluster=[], metadata={}, run_as=[], enabled=True):
            if role_name == '':
                raise EmptyValuesError("empty string in role name")
            status, role = self.get_role(role_name)
            if status.get('exists', False):
                return status, role
            role_dict = {
                "cluster": cluster,
                "indices": indices,
                "metadata": metadata,
                "run_as": run_as,
                'transient_metadata': {'enabled': enabled}
            }
            created_role = self.client.xpack.security.put_role(
                role_name, role_dict)
            x = created_role.get('role', {})
            if not x.get('created', False):
                raise NotCreated(
                    "Role was not created, repsonse: {0}".format(x))
            elif x.get('created', False):
                status, role = self.get_role(role_name)
                return {'exists': False, 'added': True}, role

        def update_role(self, role_name='', indices=[], cluster=[], metadata={}, run_as=[], enabled=True):
            if role_name == '':
                raise EmptyValuesError("empty string in role name")
            status, existing_role = self.get_role(role_name)
            if not status.get('exists', False):
                return status, existing_role

            role_dict = {
                "cluster": cluster,
                "indices": indices,
                "metadata": metadata,
                "run_as": run_as,
                'transient_metadata': {'enabled': enabled}
            }

            if existing_role == role_dict:
                return {'exists': True, 'changed': False}, existing_role
            self.client.xpack.security.put_role(role_name, role_dict)
            status, changed_role = self.get_role(role_name)
            if status.get('exists', False):
                if changed_role == role_dict:
                    return {'exists': True, 'changed': True}, changed_role

        def delete_role(self, role_name=''):
            if role_name == '':
                raise EmptyValuesError("role name is empty")
            status, role = self.get_role(role_name)
            if not status.get('exists', False):
                return status, role
            if status.get('exists', False):
                delete = self.client.xpack.security.delete_role(role_name)
                if delete.get('found', False):
                    return {'exists': True, 'deleted': True}, delete
                else:
                    return {'exists': True, 'deleted': False}, delete

    class _Users:
        def __init__(self, client):
            self.client = client.client

        def get_user(self, username=''):
            try:
                x = self.client.xpack.security.get_user(username)
                return {'exists': True}, x.get(username, '')
            except NotFoundError:
                return {'exists': False}, {}

        def add_user(self, username='', full_name='', password='', email='', roles=[], enabled=True):
            """
            return:
                when user exists:
                    {'exists': True, 'added': False}, user
                when user does not exists:
                {'exists': False, 'added': True}, self.get_user(username)
            """
            if username == '' or password == '' or email == '':
                raise EmptyValuesError(
                    "empty string in username, password or email address")

            status, user = self.get_user(username)
            if status.get('exists', False):
                return {'exists': True, 'added': False}, user.get('username', {})
            if full_name == '':
                full_name = username
            user = {
                "password": password,
                "roles": roles,
                "full_name": full_name,
                "email": email,
                "enabled": enabled,
            }

            created_user = self.client.xpack.security.put_user(username, user)
            x = created_user.get('user', {})
            if not x.get('created', False):
                raise NotCreated(
                    "User was not created, repsonse: {0}".format(x))
            elif x.get('created', False):
                status, user = self.get_user(username)
                return {'exists': False, 'added': True}, user

        def update_user(self, username='', full_name='', email='', roles=[], enabled=True):
            if username == '' or email == '':
                raise EmptyValuesError(
                    "empty string in username, password or email address")
            if full_name == '':
                full_name = username
            user = {
                "roles": roles,
                "full_name": full_name,
                "email": email,
                "enabled": enabled,
                "metadata": {},
                "username": username
            }
            status, existing_user = self.get_user(username)
            if not status.get('exists', False):
                return {'exists': False, 'changed': False}, {}
            elif status.get('exists', False):
                if existing_user == user:
                    return {'exists': True, 'changed': False}, {}
                elif existing_user != user:
                    self.client.xpack.security.put_user(username, user)
                    status, changed_user = self.get_user(username)
                    if status.get('exists', False):
                        if changed_user == user:
                            return {'exists': True, 'changed': True}, changed_user
            return {'exists': 'unknown', 'added': False}, {}

        def delete_user(self, username=''):
            if username == '':
                raise EmptyValuesError("Username is empty")
            status, user = self.get_user(username)
            if not status.get('exists', False):
                return {'exists': False, 'deleted': False}, {}
            if status.get('exists', False):
                delete = self.client.xpack.security.delete_user(username)
                if delete.get('found', False):
                    return {'exists': True, 'deleted': True}, delete
                else:
                    return {'exists': False, 'deleted': False}, delete
