#!/usr/bin/env python3
from setuptools import setup, find_packages

setup(
    name='xpack',
    version='0.1',
    python_requires='>=3.5',
    packages=find_packages(),
    install_requires=['urllib3', 'elasticsearch'],

    # Metadata
    author="Rob Haverkamp",
    author_email="rjrhaverkamp@gmail.com",
    license="Apache License 2.0",
    description="wrap elasticsearch xpack api",
    long_description=open('README.md').read(),
    url="https://github.com/rjrhaverkamp/xpack-py",
    keywords=["elasticsearch", "xpack", "cloud", "api"],
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 4 - Beta',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',

        'Operating System :: OS Independent',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    extras_require={}

)
